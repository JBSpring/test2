package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.example.domain.Vocable;


@Repository
public interface VocableRepository extends CrudRepository<Vocable, Long> {

	
	List<Vocable> findAll();
	
	@Query(value = "select word from Vocable", nativeQuery = true)
	List<String> getVocable();
	
	@Query(value = "select mean from Vocable", nativeQuery = true)
	List<String> findMean();

	//Todo
	@Query(value = "select id from Vocable", nativeQuery = true)
	List<String> findId();
}
