package com.example.demo.controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.domain.Vocable;
import com.example.repository.VocableRepository;
import com.example.service.VocableService;

/*
 * TODO ne lehessen ugyan azt az elemet felvinni megint
 * TODO error message, save üres mezőkkel miért nem működik - ?-
 * TODO random az aktuális elemet ne tudja visszaadni
 * 
 */


@Controller
public class HelloWorldController {

	private VocableService vocableService;

	@Autowired
	public void setVocableService(VocableService vocableService) {
		this.vocableService = vocableService;
	}

	@Autowired
	public VocableRepository vocaRepo;

	@Resource
	private ConversionService conversionService;

	private static final Logger logger = LoggerFactory.getLogger(HelloWorldController.class);
	
	@RequestMapping("/voca")
	public String voca(Model model) {
		model.addAttribute("vocable", vocableService.getVocable());
		//model.addAttribute("vocables", vocableService.getRandomWord().toString()); 
		return "voca";
	}

	@RequestMapping("/")
	public String index(Model model) {
		return "index";
	}

	@RequestMapping("/random")
	public String randomWord(Model model) {
		model.addAttribute("randomVocable", vocableService.getRandomWord());
		
		Vocable proba = vocableService.getRandomWord();
		model.addAttribute("vocableWordTest", proba.getWord());
		model.addAttribute("vocablesMeanTest", proba.getMean());
		
		return "random";
	}

	@GetMapping("/input")
	public String vocableForm(Model model) {
		model.addAttribute("post", new Vocable());
		return "input";
	}

	@PostMapping("/input")
	public String saveNewWord(@ModelAttribute("post") 
	@Valid Vocable vocValid, @RequestParam(name = "word") String word, 
	@RequestParam(name = "mean") String mean, 
			 BindingResult br) {
		
		if (br.hasErrors()) {
			logger.info("Validation errors while submitting form."); // idáig sem jut el
			br.reject("input.error.userInput");
			return "input";
		}
		
		Vocable uj = new Vocable();
		uj.setMean(mean);
		uj.setWord(word);

		vocaRepo.save(uj);
		return "redirect:/";
	}
}
